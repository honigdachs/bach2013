
# PlotCSV - v1.0.0-beta

A CSV file plotting application with Python, D3, MongoDB and NVD3

This is a very early release. More plotting options and other functionalities will be included in the futere

---

# Current development focus

-Error handling 
-Better documention
-Bug fixes all around

---

# Installation Instructions

In order to be able to run this application the following requirements are needed: 

- Python: http://www.python.org/getit/
Version 2.7.x is recommended

- Bottle: This is a simple Web Framework for Python. http://bottlepy.org/docs/dev/

- MongoDB: http://www.mongodb.org/downloads

- sleepy mongoose: This is a mongodb REST Server for HTTP requests: https://github.com/10gen-labs/sleepy.mongoose/wiki

- D3, Twitter Bootstrap and NVD3 are already included in the sources and no further installation is needed. 

# Howto Run  

- run mongodb on on port 3141:
eg. : mongod --dbpath . --port 3141

- connect sleepy mongose at port 3141. 
eg: curl --data server=localhost:3141 'http://localhost:27080/_connect'

- go to the source and run bottle_backend.py
eg: your user$ python bottle_backend.py 

- open a browser and go to localhost:8080

Enjoy! :) 

---



## (Officially) Supported Browsers

* Chrome latest version 
* Firefox latest version


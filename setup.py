# -*- coding: utf-8 -*-
# always add encoding at the beginning
'''
Created on 12.03.2013

@author: matyas


# add information of the setup.py

# e.g.

# 

Description of the package.

import setuptools

setuptools.setup (
    name = 'PlotCSV',
    author = 'Matyas Karacsonyi',
    author_email = ['matthias.karacsonyi@gmail.com'] 
                    
    version = '1.0.0',
    packages = setuptools.find_packages('src'),
    package_dir={'':'src'},
    install_requires = [],
    scripts = [])


'''
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 11 23:37:37 2013

@author: matyas
"""


"""
import rpy2.robjects as robjects
import time

x = robjects.FloatVector((1,2,3))
y = robjects.FloatVector((2,3,4))


res = robjects.r['chisq.test'](x, y)

plot= robjects.r['barplot'](x)

print res
print plot

time.sleep(5)
"""
import pandas as pd
import numpy as np

s = pd.Series([1,3,5,np.nan,6,8])
print s

dates = pd.date_range('20130101',periods=6)
print dates

df = pd.DataFrame(np.random.randn(6,4),index=dates,columns=list('ABCD'))

print df
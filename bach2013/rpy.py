# -*- coding: utf-8 -*-
"""
Created on Thu Jul 11 23:37:37 2013

@author: matyas
"""
import rpy2.robjects as robjects


x = robjects.FloatVector((1,2,3))
y = robjects.FloatVector((2,3,4))

res = robjects.r['chisq.test'](x, y)

print res
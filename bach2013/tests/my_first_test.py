'''
Created on 12.03.2013

@author: matyas
'''
import unittest


class TestKalifragi(unittest.TestCase):


    def setUp(self):
        pass


    def tearDown(self):
        pass


    def testName(self):
        import math
        assert math.pow(2,2) == 4
        self.assertEquals(math.pow(2,2), 4)
        
    def test_kalifragi(self):
        import math
        x = 3
        power = 2
        result = 10
        assert math.pow(x, power) == result, "The variables are %s and %s" % (x, power)


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
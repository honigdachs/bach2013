# -*- coding: utf-8 -*-
'''
Created on 20.03.2013
description missing
@author: matyas
'''

# added documentation


# changed import style
# http://www.python.org/dev/peps/pep-0008/#imports
import collections
import urllib2
import urllib
import json
import os
from bottle import route, run, debug, template, request, static_file
from bottle import error


from controller import CsvParser


ParseObject = CsvParser()
# The Parserobject from controller.py is created here

global_searchresult = {}
'''
When a user performs a certain search query over the web interface the result
is saved in this variable in order to be able to access it in other functions
for the plotting process
'''

"""Defining Static Paths here"""


# maybe put them in one central configuration file.
@route('/css/:filename')
def server_css(filename):
    return static_file(filename, root='./css/')


@route('/img/:filename')
def server_img(filename):
    return static_file(filename, root='./img/')


@route('/javascript/:filename')
def server_js(filename):
    return static_file(filename, root='./js/')


@route('/less/:filename')
def server_less(filename):
    return static_file(filename, root='./less/')


@route('/', method='GET')
def index_page():
    '''
    Returns the Start Page where a Document for Parsing can be selected
    '''
    return template('views/index.tpl')


@route('/upload', method='POST')
def do_upload():
    '''
    checks validity of the Document chosen at the Start Page, and if valid
    parses the document into the MongoDB database, and shows the data with
    the data_table template


    :param data: A local file that gets selected over the web interface
    :type client:
     etc.
    '''

    data = request.files.data
    # checking whether the file is valid
    # confusing control structure - try to work with simple if statements
    # avoid if - else when possible done.
    if data and data.file:

        name, ext = os.path.splitext(data.filename)
        if ext not in ('.csv', '.xls'):
            return 'File extension not allowed.:', ext

        if ext in('.csv', '.xls'):  # file extension is accepted

            raw = data.file.read()  # This is dangerous for big files

            # creating temporary file for the csv Reader
            with open('csv_to_parse.csv', 'w') as f:
                f.write(raw)

            filename = os.path.join(os.getcwd(), 'csv_to_parse.csv')

            # the object method parses the data into the database here
            ParseObject.parsetodb(filename)

            # Reading and parsing elements from db to template
            output = template('views/data_table.tpl',
                              resultlist=make_query(""),
                              tableheaders=get_rownames())
            return output

    else:  # if not data and data.file
        return "Please select a local file."


@route('/showdata')
def show_data():
    '''
    Shows all elements of the database in an HTML Table
    '''
    output = template('views/data_table.tpl',
                      resultlist=make_query(""),
                      # the parameter ("") returns the whole database and sets
                      # the global variable global_searchresult to the
                      # make_query() result.
                      tableheaders=get_rownames())

    return output


@route('/chooseplot')
def chooseplottype():
    '''
    Shows a template with different types of charts
    '''

    output = template('views/chooseplot.tpl')
    return output


@route('/choosebarvars')
def read_barchart_varnames():
    '''
    Depending on the chosen plot type return checkboxes with the variable
    names and values that can be chosen for the plot
    '''

    output = template('views/selectbaratts.tpl',
                      varlist=get_rownames_from_database())
    return output


@route('/drawbarplot')
def choose_bar_vars():
    '''
    Draws a Barchart with the configuration chosen in the
    read_barchart_varnames method, by returning the necessary variables
    to the template
    '''

    heightvar = request.query.get('var_numselection')
    namevar = request.query.get('var_nameselection')

    output = template('views/drawbarchart.tpl', d3heightvar=heightvar,
                      d3namevar=namevar,
                      resultlist=convert(global_searchresult['results']))

    return output


@route('/choosescattervars')
def read_scatterplot_varnames():
    '''
    Depending on the chosen plot type return checkboxes with the variable
    names and values that can be chosen for the plot
    '''

    output = template('views/selectscattats.tpl',
                      varlist=get_rownames_from_database())
    return output


@route('/choosepiechartvars')
def choose_piechart_vars():
    """
    Shows a page with a selection of possible attributes for the Pie Chart
    """
    output = template('views/selectpiechartatts.tpl',
                      varlist=get_rownames_from_database())
    return output


@route('/drawscatterplot')
def draw_scatterplot():
    '''
    Draws a Barchart with the configuration chosen in the
    read_scatterplot_varnames method, by returning the necessary variables
    to the template
    '''

    numvar1 = request.query.get('var_att1selection')
    numvar2 = request.query.get('var_att2selection')

    output = template('views/drawscatterplot.tpl', d3attvar1=numvar1,
                      d3attvar2=numvar2,
                      resultlist=convert(global_searchresult['results']),
                      length=len(global_searchresult['results']))

    return output


@route('/drawpiechart')
def show_piechart():
    """
    shows a Pie Chart Plot
    """

    pie_namevar = request.query.get('var_pienameselection')
    pie_valuevar = request.query.get('var_pievalueselection')

    output = template('views/drawpiechart.tpl',
                      d3namevar=pie_namevar,
                      d3valuevar=pie_valuevar,
                      resultlist=convert(global_searchresult['results']))
    return output


@route('/performesearch')
def show_search_results():
    '''
    Returns the MongoDB Search Resuls for the query which was given by the user
    over the Web Interface.
    '''
    command = request.query.get("searchquery")
    output = template('views/mongooutput.tpl',
                      resultlist=make_query(command),
                      tableheaders=get_rownames())

    return output


@route('/showme')
def show_table():
    '''
    Takes the rownames which were chosen by the user over the Web Interface and
    displays the desired table in a template

    params: global_searchresult:
    this is a the global variable where the results of the user's search
    queries are saved. if no specific queries are performed the whole database
    will be saved in this variable.

    params: attributes:
    saves the selection of rownames which the user selected in the Web
    Interface
    '''
    attributes = request.query.getall("atts_selection")

    output = template('views/data_table_selected.tpl',
                      resultlist=global_searchresult,
                      tableheaders=get_rownames(),
                      selectedrows=attributes)

    return output


@route('/about')
def about_page():
    """
    shows a page with information and future plans about the project
    """
    output = template('views/about.tpl')
    return output


@route('/simplebarchart')
def show_bar_chart():
    """
    shows a page with a bar chart
    """
    output = template('views/simplebarchart.tpl',
                      resultlist=convert(global_searchresult['results']))
    return output


@route('/scatterplot')
def scatterplot():
    """
    shows a page with information and future plans about the project
    """
    output = template('views/scatterplot_new.tpl',
                      resultlist=convert(global_searchresult['results']),
                      length=len(global_searchresult['results']))
    return output


@route('/removedocument')
def remove_data_from_db():
    """
    Removes all data of the current collection. This action is not undoable.
    """
    ParseObject.db.csvcollection.remove()
    output = template('views/index.tpl')
    return output


@error(404)
def error404(error):
    """
    redirects to a page not found template instead of raising an error
    """
    output = template('views/error404.tpl')
    return(output)


#Defining Functions here


def get_rownames_from_database():
    '''
    This function makes a database Query for all the Objects in the database,
    iterates over the first cursor object and saves the key elements into a
    list

    >>> isinstance(get_rownames_from_database(),list)
    True


    '''

    headerlist = []
    headercursor = ParseObject.db.csvcollection.find({}, {"_id": 0})
    for rownames in headercursor:  # these are the rownames from the csv file
        headerlist.append(list(rownames))
        break  # breaks because the rownames are already in the first cursor
               # element, no further iteration needed

    #return sorted(headerlist.iteritems())
    return headerlist


def convert(data):
    '''
    Takes a Dictionary with unicoded keys and values, and returns them
    in cleaned form. (Without unicode strings) This is necessary because the
    results of the search queries are returned in unicode and javascript has
    problems with processing this encoding.

    >>> isinstance(convert("teststring"),object)
    True
    '''
    try:

            if isinstance(data, basestring):
                return str(data)
            elif isinstance(data, collections.Mapping):
                return dict(map(convert, data.iteritems()))
            elif isinstance(data, collections.Iterable):
                return type(data)(map(convert, data))
            else:
                return data
    except: pass


def make_query(query):
    '''
    Takes a query parameter from the user over the Web Interface and performs
    a search query at the MongoDB Rest Server. The query is performed over
    a httpy request and the query is encoded with the urllib.quote function.
    The result of the query gets parsed and saved in a JSON object. This JSON
    Object getssaved in the global variable global_searchresult to make it
    accessible to other functions.

    >>> isinstance(make_query(""),object)
    True
    '''

    url = 'http://localhost:27080/plotcsvdb/csvcollection/_find?criteria='

    #Sleepy Mongoose needs to be running tu be able  to run the query

    params = query  # The search query comes from the Browser
    # The search query is handed to to a MongoDB REST interface.

    hide_id_param = ';fields={"_id":0}'
    #makes sure that the _id field from the MongoDB databses is not returned

    return_all_fields = ';batch_size=100000'
    # sleepy Mongoose (the REST API) only returns 15 fields by default when
    # using the search operator with this parameter we make sure that at least
    # 10000 documents will be sent back.

    request = urllib2.Request(url + urllib.quote(params) + hide_id_param + return_all_fields, None)
    #Escape special Characters of the params attribute for the HTML Query

    response = urllib2.urlopen(request)

    query_results = json.load(response)
    #Loads the retrieved HTML Page into a JSON Object

    #cleans the unicode strings in the JSON Object
    global global_searchresult

    global_searchresult = query_results

    return query_results


def get_rownames():
    '''
    Takes a query parameter from the user over the Web Interface and performs
    a search query at the MongoDB Rest Server. The query is performed over
    a httpy request and the query is encoded with the urllib.quote function.
    The result of the query gets parsed and saved in a JSON object. This JSON
    Object getssaved in the global variable global_searchresult to make it
    accessible to other functions.

    >>> isinstance(make_query(""),object)
    True
    '''

    url = 'http://localhost:27080/plotcsvdb/csvcollection/_find?criteria='

    #Sleepy Mongoose needs to be running tu be able  to run the query

    hide_id_param = ';fields={"_id":0}'
    #makes sure that the _id field from the MongoDB databses is not returned

    return_one_field = ';batch_size=1'
    # sleepy Mongoose (the REST API) only returns 15 fields by default when
    # using the search operator with this parameter we make sure that at least
    # 10000 documents will be sent back.

    request = urllib2.Request(url + hide_id_param + return_one_field, None)
    #Escape special Characters of the params attribute for the HTML Query

    response = urllib2.urlopen(request)

    query_results = json.load(response)
    #Loads the retrieved HTML Page into a JSON Object

    #cleans the unicode strings in the JSON Object

    keylist = []
    for each_element in query_results['results']:
        keylist.append(each_element)

    keylist.sort

    return keylist

if __name__ == '__main__':
    import doctest
    doctest.testmod()
    debug(True)

    run(host='localhost', port=8080, reloader=True)

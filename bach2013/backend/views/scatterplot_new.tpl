<!DOCTYPE html>
<meta charset="utf-8">

<link href="css/nv.d3.css" rel="stylesheet" type="text/css">

<style>

body {
  overflow-y:scroll;
  margin: 0;
  padding: 0;
}

svg {
  overflow: hidden;
}

div {
  border: 0;
  margin: 0;
}

/*
#offsetDiv {
  margin-left: 100px;
  margin-top: 100px;
}
*/


#test1 {
  margin: 0;
}

#test1 svg {
  height: 500px;
}

</style>

<body>

<div id="offsetDiv">
  <div id="test1" class="chartWrap">
    <svg></svg>
  </div>
</div>

<script src="javascript/d3.v3.js"></script>
<script src="javascript/fisheye.js"></script>
<script src="javascript/nv.d3.js"></script>
<script src="javascript/tooltip.js"></script>
<script src="javascript/utils.js"></script>
<script src="javascript/models/legend.js"></script>
<script src="javascript/models/axis.js"></script>
<script src="javascript/distribution.js"></script>
<script src="javascript/scatter.js"></script>
<script src="javascript/scatterChart.js"></script>
<script>

var dataset_bottle ={{!resultlist}}
shapes = ['circle', 'cross', 'triangle-up', 'triangle-down', 'diamond', 'square'];

var mydata = [
{key : 'chosen data',
values : []
}
];

for (i=0;i<{{!length}};i++)
	{
		
		  mydata[0].values.push({
        x: dataset_bottle[i].{{!d3attvar1}}, 
        y: dataset_bottle[i].{{!d3attvar2}}, 
        size: (dataset_bottle[i].{{!d3attvar1}} + dataset_bottle[i].{{!d3attvar2}})/2/10, 
        shape: 'cross'
      });		
		//console.log(dataset_bottle[i].AvTemp1995);
	}
console.log(mydata);


//Format A
var chart;
nv.addGraph(function() {
  chart = nv.models.scatterChart()
                .showDistX(true)
                .showDistY(true)
                //.height(500)
                .useVoronoi(true)
                .color(d3.scale.category10().range());

  chart.xAxis.tickFormat(d3.format('.02f'))
  chart.yAxis.tickFormat(d3.format('.02f'))

  d3.select('#test1 svg')
      .datum(mydata)
    .transition().duration(500)
      .call(chart);

  nv.utils.windowResize(chart.update);

  chart.dispatch.on('stateChange', function(e) { ('New State:', JSON.stringify(e)); });

  return chart;
});







</script>

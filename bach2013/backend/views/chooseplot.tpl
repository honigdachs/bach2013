<html>


    <head>
        <link type="text/css" rel="stylesheet" href="css/bootstrap.css">
        <script type="text/javascript" src="javascript/jquery1.10.0.js"></script>         
        <script type="text/javascript" src="javascript/bootstrap.js"></script>        
        <script type="text/javascript" src="javascript/d3.v3.js"></script> 
        
         					                  								     
								      
								      
    </head>

    <body>
        
            <ul class="nav nav-tabs nav-tabs span10">
                <li ><a href="/">Home </a></li>
                <li><a href="/showdata">Your File</a></li>                
                <li class="active"><a href="/chooseplot">Choose Plot Type</a></li>   
                <li><a href="/about">About</a></li>                      
                
            
            
            </ul> 
          
          <br>
      
           <form class="well span9" action="/drawplot" >
                <h3>Plotting Options</h3> <br> <br>              
               <span class="label label-success">Bar Chart</span>    <span class="label label-success pull-right">Scatterplot</span>          
               <p>
               <a href="/choosebarvars"><img src="/img/barchart.jpg" class="img-rounded" alt="barchart"></a>    
               <a href="/choosescattervars" ><img  height="100" width="250" align="right" src="/img/scatterplot.png" class="img-rounded"  alt="scatterplot"></a> 
               </p>         
 					<p>
					<span class="label label-success">Pie Chart</span> 	 					
 					<br> <br>
									
					<a href="/choosepiechartvars" ><img  height="100" width="250" align="center" src="/img/piechart.png" class="img-rounded"  alt="pie chart"></a>              
					</p>         
          </form>                
               
               
               
                   
                   
           
   
   </body>   
</html>  
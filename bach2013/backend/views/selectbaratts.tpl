﻿<!DOCTYPE html>
<html>
	<head>
		<title>Your File</title>
		
		<link rel="stylesheet" href="css/bootstrap-2.3.2.min.css" type="text/css">
		<link rel="stylesheet" href="css/bootstrap-multiselect.css" type="text/css">
		<link rel="stylesheet" href="css/prettify.css" type="text/css">

		<script type="text/javascript" src="javascript/jquery-2.0.1.min.js"></script>
		<script type="text/javascript" src="javascript/bootstrap-2.3.2.min.js"></script>
		<script type="text/javascript" src="javascript/bootstrap-multiselect.js"></script>
		<script type="text/javascript" src="javascript/prettify.js"></script>
		<script type="text/javascript" src="javascript/d3.v3.js"></script> 
	</head>
	<body>
        
            <ul class="nav nav-tabs nav-tabs span10">
                <li ><a href="/">Home </a></li>
                <li><a href="/showdata">Your File</a></li>                
                <li><a href="/chooseplot">Choose Plot Type</a></li> 
                <li class="active" ><a href="/choosebarvars">Barchart </a></li>
                <li ><a href="/choosescattervars">Scatterplot </a></li>                 
               
            
            </ul> 
          

         
           <form class="well span9" action="/drawbarplot" >
                            
                        
                <script>
	            $('.dropdown input, .dropdown label').click(function (event) {
	                event.stopPropagation();
	            });
        	</script>




		
		<script type="text/javascript">
			    $(document).ready(function() {
			        window.prettyPrint() && prettyPrint();
					
			        $('#example1').multiselect();
			        
			        $('#example2').multiselect();

			        $('#example27').multiselect({
			        	includeSelectAllOption: true,
			        	enableFiltering: true
			        });

					
			    });
    	</script>    
                     <span class="label label-success">Please select an integer and a text value for the Bar Chart</span>  <br><br>        
               <br>
               
               
               <select name="var_numselection" id="example1" style="display: none;">		
               %for all_elements in varlist:               
                    %for element in all_elements:                    
                   <option name="att_selection" value = {{element}}   >  
                             <span>{{element}}</span> <br><br>            
                    %end
               %end 
               </select>	
					<img src="/img/arrow_right.jpg" class="img-rounded" alt="arrow_right">   <img src="/img/numbers.jpg" width="130" height="130" class="img-rounded" alt="arrow_right"> 					
					
									
					<br>               
                <span class="label">Please select a variable with an INTEGER value for the height of the Bar Charts </span> <br>          
               <br>
        			
        			
               <select name="var_nameselection" id="example2" style="display: none;">		
					             
               %for all_elements in varlist:               
                    %for element in all_elements:                    
                   <option name="att_selection_text" value = {{element}}   >  
                             <span>{{element}}</span> <br><br>            
                    %end
               %end 
               </select>	
					<img src="/img/arrow_right.jpg" class="img-rounded" alt="arrow_right">  <img src="/img/text.jpg" width="115" height="110" class="img-rounded" alt="text">  					
					<br>               
               <span class="label label">Please select a variable with a TEXT value for the Bar names </span> <br>                
               <br>
					<br>
					<img src="/img/barchart.jpg"  width="130" height="130" class="img-rounded pull right" alt="barchart">	               
               <button type="submit" value="Submit" class="btn-warning">Draw Bar Chart!</button>
          </form>                
               
               
               
                   
                   
           
   
   </body>   
</html>  
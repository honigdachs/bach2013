<!DOCTYPE HTML>
<html>


    <head>
        <link type="text/css" rel="stylesheet" href="css/bootstrap.css">
         
    </head>

    <body>



        
            <ul class="nav nav-tabs nav-tabs span10">
                <li ><a href="/">Index </a></li>
                <li ><a href="/showdata">Your File </a></li>
                <li><a href="/about">About </a></li>
                <li><a href="/plots">Plots </a></li>
                <li><a href="/interactive">Interactive </a></li>
                <li class="active"><a href="/sendform">Sendform </a></li>
                <li ><a href="/dropdown">Dropdown </a></li>
            
            </ul>       


  <br>
 


      <div>
          <form class="well span12" name="input" action="/sendform" method="POST">  
          <br> 
          <label>Choose Attribute (Row Name)</label>  
          <input name="variable" type="text" class="span3" placeholder="Type something…">  
          <span class="help-inline">Associated help text!</span>  
           
          <button type="submit" value="Submit" class="btn">Submit</button>  
          </form>  
     </div>
     
     
     <div>
          <form class="well span12">  
          
          	<script type="text/javascript">
          var dataset = [
        { apples: 5, oranges: 10, grapes: 22 },
        { apples: 4, oranges: 12, grapes: 28 },
        { apples: 2, oranges: 19, grapes: 32 },
        { apples: 7, oranges: 23, grapes: 35 },
        { apples: 23, oranges: 17, grapes: 43 }
          ];
           
           </script>         
         
          </form>  
     </div>

     
   
   
   </body>
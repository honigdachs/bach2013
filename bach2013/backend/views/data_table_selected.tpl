﻿<!DOCTYPE html>
<html>
	<head>
		<title>Your File</title>
		
		<link rel="stylesheet" href="css/bootstrap-2.3.2.min.css" type="text/css">
		<link rel="stylesheet" href="css/bootstrap-multiselect.css" type="text/css">
		<link rel="stylesheet" href="css/prettify.css" type="text/css">

		<script type="text/javascript" src="javascript/jquery-2.0.1.min.js"></script>
		<script type="text/javascript" src="javascript/bootstrap-2.3.2.min.js"></script>
		<script type="text/javascript" src="javascript/bootstrap-multiselect.js"></script>
		<script type="text/javascript" src="javascript/prettify.js"></script>
	</head>
	<body>
 
 
 		<ul class="nav nav-tabs nav-tabs span10">
                <li><a href="/">Home </a></li>                
                <li class="active"><a href="/showdata">Your File</a></li>                
                <li><a href="/chooseplot">Choose Plot Type</a></li>
                <li><a href="/about">About</a></li>  
               
                                
           
               
            
            </ul>  
 
<form class="well form-seach span12" action="/chooseplot">	   
   
 
 <div class="container">
 <button type="submit" value="Submit" class="btn-success pull-right" >Plot this specific data!</button>	
	        <script>
	            $('.dropdown input, .dropdown label').click(function (event) {
	                event.stopPropagation();
	            });
        	</script>




		
		<script type="text/javascript">
			    $(document).ready(function() {
			        window.prettyPrint() && prettyPrint();
					
			    

			        $('#example27').multiselect({
			        	includeSelectAllOption: true,
						enableFiltering: true			        
			        });

					
			    });
			</script>

						<p>
				 	<span class="label label-success">This is your data. If the parsing process was successfull you should see your Data now in a table below</span>
			</p>
			</form>	
			

			   			
			<table class="table table-striped">
				<tr>
					<td>
					<form  action="/performesearch">
						<input name="searchquery" placeholder="MongoDB Command...">
    					<button class="btn-danger">Search!</button>
					</form>	   	           
               
           
           <form action="/removedocument">					
					<td>
						<p class="alert alert-info">
							<b>wrong file?</b> <button type="submit" value="Submit" class="btn-danger" >Delete this file!</button>	 
						</p>
					</td>
				</form> 
				
				<br>
				
								 
         <form action="/showme">						
					<select name="atts_selection" id="example27" multiple="multiple">					 
					 %for all_elements in tableheaders:               
                    %for element in all_elements:                    
                    <option name="att_selection" value = {{element}}   >  
                             <span>{{element}}</span> <br><br>             
                    %end
               %end 					 
					</select>					 
					 
					<p class="alert alert-info">
					 <b>Note: Which rownames should be shown?</b> <button type="submit" value="Submit" class="btn-success" >Show me!</button>						
					</p>					
					</td>

		
		<table class="table table-striped">
    <thead>
        <tr>
           %for elements in tableheaders:
               %for each_element in elements:
               	%if each_element in selectedrows:
                		<th>{{each_element}}</th>
                %end
           %end
        </tr>
    </thead>
        

    
    <tbody>   
	
	%for each_element in resultlist['results']:
		<tr>
								
		%for col in each_element:
			%if col in selectedrows:
				<td>{{each_element[col]}}</td>	
			%end			
			
		%end
											
		</tr>
	%end	
    <tbody>
</table>

     <script type="text/javascript">
     var rownames={{!tableheaders}}
     </script>		
		</div>
		</form>		
		</form>
		
	</body>
</html>

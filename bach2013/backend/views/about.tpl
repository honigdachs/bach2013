<html>


    <head>
        <link type="text/css" rel="stylesheet" href="css/bootstrap.css">
        <script type="text/javascript" src="javascript/d3.v3.js"></script> 
        <script type="text/javascript" src="javascript/jquery1.10.0.js"></script> 
         					                  								     
								      
								      
    </head>

    <body>
        
            <ul class="nav nav-tabs nav-tabs span10">
                <li><a href="/">Home </a></li>                
                <li><a href="/showdata">Your File</a></li>                
                <li><a href="/chooseplot">Choose Plot Type</a></li>   
                <li class="active"><a href="/about">About</a></li>   
            
            </ul>      
   
<!--           --> 
    
      <div>
                    <form class="well span14">
                       <h1>What's next?</h1>
                        <span class="label label-success">This is a very early release and a lot of error handling has yet to be implemented </span>                       
                       <h3>The following features are planned to be implemented in the future:</h3> 
                       <ul>
                         <span class="label label-info"><li>More Plots Types, and more customizing (eg. Axes)</li></span>
                         <br> <br>
                         <span class="label label-info"><li>Statistical Analysis with R powered by the Rpy module</li></span>
                         <br><br>
                         <span class="label label-info"><li>User Profiles where Data and Plots can be saved</li></span>                      
                       
                       </ul> 
                       <br>	
                       <h3>Powered By</h3>                       
                       <span class="label label-important ">D3.js:</span><span class="label label-important pull-right ">Python:</span>   <br>                    
                       <p><img class="img-rounded pull-right" src="/img/python.jpg" alt="future">   <img class="img-rounded pull-right" src="/img/bootstrap.jpg" class="img-rounded" alt="Python">  <img class="pull-right" src="/img/mongodb.jpg" class="img-rounded" alt="MongoDB"><img class="pull-right" src="/img/d3.png" alt="D3"></p>   
                       <p></p>
     
                </form> 
          </div>    
   
       
          
   </body>
   
</html>
<!DOCTYPE html>
<meta charset="utf-8">
<link href="css/nv.d3.css" rel="stylesheet" type="text/css">
<link type="text/css" rel="stylesheet" href="css/bootstrap.css">
<style>

body {
  overflow-y:scroll;
}

text {
  font: 12px sans-serif;
}

.mypiechart {
  width: 500px;
  border: 2px;
}
</style>
<body>

            <ul class="nav nav-tabs nav-tabs span10">
                <li><a href="/">Home </a></li>                
                <li><a href="/showdata">Your File</a></li>                
                <li><a href="/chooseplot">Choose Plot Type</a></li>
                <li><a href="/about">About</a></li>  
                <li class="active"><a href="/piechart">Pie Chart</a></li>
                                      
           
               
            
            </ul>       

<form class="well form-seach span12">
<h2>Pie Chart</h2>
<svg id="test1" class="mypiechart"></svg>



<script type="text/javascript" src="javascript/jquery-2.0.1.min.js"></script>

<script src="javascript/d3.v3.js"></script>

<script src="javascript/nv.d3.js"></script>
<script src="javascript/legend.js"></script>
<script src="javascript/pie.js"></script>
<script src="javscript/pieChart.js"></script>
<script src="javascript/utils.js"></script>
<script>

var dataset_bottle={{!resultlist}};
 


nv.addGraph(function() {
    var width = 500,
        height = 500;

    var chart = nv.models.pieChart()
        .x(function(d) { return d.{{d3namevar}} })
        .y(function(d) { return d.{{d3valuevar}} })
        //.showLabels(false)
        .values(function(d) { return d })
        .color(d3.scale.category10().range())
        .width(width)
        .height(height);

      d3.select("#test1")
          .datum([dataset_bottle])
        .transition().duration(1200)
          .attr('width', width)
          .attr('height', height)
          .call(chart);

    chart.dispatch.on('stateChange', function(e) { nv.log('New State:', JSON.stringify(e)); });

    return chart;
});

nv.addGraph(function() {

    var width = 500,
        height = 500;

    var chart = nv.models.pieChart()
        .x(function(d) {
		  try {
    			if (typeof {{!d3namevar}} === 'None')	{}}

catch (e) {}             
        
         return {{!d3namevar}}})
        //.y(function(d) { return d.value })
        .values(function(d) { 
        
			try {
    			if (typeof d === 'None')	{}}

catch (e) {}        
        
        return parseFloat(d) })
       
        //.labelThreshold(.08)
        //.showLabels(false)
        .color(d3.scale.category10().range())
        .width(width)
        .height(height)
        .donut(true);

    chart.pie
        .startAngle(function(d) { return d.startAngle/2 -Math.PI/2 })
        .endAngle(function(d) { return d.endAngle/2 -Math.PI/2 });

      //chart.pie.donutLabelsOutside(true).donut(true);

      d3.select("#test2")
          //.datum(historicalBarChart)
          .datum([dataset_bottle])
        .transition().duration(1200)
          .attr('width', width)
          .attr('height', height)
          .call(chart);

    return chart;
});

</script>

</form>

<!DOCTYPE html>
<meta charset="utf-8">
<html>

<head>
        
<link type="text/css" rel="stylesheet" href="css/bootstrap.css">            
<link href="css/nv.d3.css" rel="stylesheet" type="text/css">

<style>

body {
  overflow-y:scroll;
}

text {
  font: 12px sans-serif;
}

svg {
  display: block;
}

#chart1 svg{
  height: 500px;
  min-width: 100px;
  min-height: 100px;
/*
  margin: 10px;
  Minimum height and width is a good idea to prevent negative SVG dimensions...
  For example width should be =< margin.left + margin.right + 1,
  of course 1 pixel for the entire chart would not be very useful, BUT should not have errors
*/
}

</style>

</head>
<body>

<ul class="nav nav-tabs nav-tabs span10">
                <li><a href="/">Home </a></li>                
                <li><a href="/showdata">Your File</a></li>                
                <li><a href="/chooseplot">Choose Plot Type</a></li>
                <li><a href="/about">About</a></li>  
                <li class="active"><a href="/simplebarchart">Simple Bar Chart</a></li>
                                      
           
               
            
</ul>    

<form class="well">
<br><br>
  <div id="chart1">
    <svg></svg>
  </div>

<script src="javascript/d3.v3.js"></script>
<script src="javascript/nv.d3.js"></script>
<!-- including all the components so I don't have to minify every time I test in development -->
<script src="javascript/tooltip.js"></script>
<script src="javascript/utils.js"></script>
<script src="javascript/axis.js"></script>
<script src="javascript/discreteBar.js"></script>
<script src="javascript/discreteBarChart.js"></script>
<script>

plotattributes = [ 
  {
    key: "My Attributes",
    values: {{!resultlist}}
      }
      ];





historicalBarChart = [ 
  {
    key: "Cumulative Return",
    values: [
      { 
        "label" : "A" ,
        "value" : 29.765957771107
      } , 
      { 
        "label" : "B" , 
        "value" : 0
      } , 
      { 
        "label" : "C" , 
        "value" : 32.807804682612
      } , 
      { 
        "label" : "D" , 
        "value" : 196.45946739256
      } , 
      { 
        "label" : "E" ,
        "value" : 0.19434030906893
      } , 
      { 
        "label" : "F" , 
        "value" : 98.079782601442
      } , 
      { 
        "label" : "G" , 
        "value" : 13.925743130903
      } , 
      { 
        "label" : "H" , 
        "value" : 5.1387322875705
      }
    ]
  }
];

var testdata = [ 
  {
    key: "My Attributes",
    values: [{'Country': 'Austria', 'AvTemp2013': '36', 'AvTemp1995': '29'}, {'Country': 'Italy', 'AvTemp2013': '27', 'AvTemp1995': '35'}, {'Country': 'Germany', 'AvTemp2013': '27', 'AvTemp1995': '23'}, {'Country': 'Sweden', 'AvTemp2013': '38', 'AvTemp1995': '38'}, {'Country': 'Spain', 'AvTemp2013': '24', 'AvTemp1995': '24'}, {'Country': 'Colombia', 'AvTemp2013': '27', 'AvTemp1995': '33'}, {'Country': 'Portugal', 'AvTemp2013': '30', 'AvTemp1995': '25'}, {'Country': 'Hungary', 'AvTemp2013': '38', 'AvTemp1995': '32'}]}
      ];


console.log(testdata)
console.log("-------------------------------------------")
console.log(plotattributes)


nv.addGraph(function() {  
  var chart = nv.models.discreteBarChart()
      .x(function(d) { return d.{{!d3namevar}} })
      .y(function(d) { return parseFloat(d.{{!d3heightvar}}) })
      .staggerLabels(true)
      //.staggerLabels(historicalBarChart[0].values.length > 8)
      .tooltips(false)
      .showValues(true)

  d3.select('#chart1 svg')
      .datum(plotattributes)
    .transition().duration(500)
      .call(chart);

  nv.utils.windowResize(chart.update);

  return chart;
});


</script>
</form>
</body>
</html>

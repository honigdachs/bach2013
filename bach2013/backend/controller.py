# -*- coding: utf-8 -*-
'''

This module takes a CSV Document, reads its content, alters the fieldnames
to replace blank lines with underscores and loads its content line by line into
a MongoDB database that is created in the intit function of the class.

Requires a path to the csv document.

@author: matyas # follow rest documentation standard.
'''

'''
To implement: remove special characters

import re
special_chars = '&.!?ÄÜÖäüöß'

def remove_chars(s):
    return ''.join(re.findall('[^' + special_chars + ']', s))
'''

import csv

from pymongo import MongoClient


class CsvParser(object):
    '''
    Parses a CSV File into a MongoDB Database. The filepath is needed for
    the creation of the object.
    '''
    client = MongoClient(host='localhost', port=3141)
    db = client.plotcsvdb

    def __init__(self, client=MongoClient(host='localhost', port=3141),
                 db=client.plotcsvdb):

        '''
        The database connection to MongoDB gets established at port 3141
        and a database is created with the name plotcsvdb

       :param client: a pymongo MongoClient that connects to the port at 3141
       :type client: <class 'pymongo.mongo_client.MongoClient'>

       :param db: creates a MongoDB database with the name plotcsvdb.
                   This database will be used to store all the Information
       :type db: <class 'pymongo.database.Database'>

        >>> isinstance(CsvParser.db,object)
        True
        >>> isinstance(CsvParser.client,object)
        True
        '''
        self.client = client
        self.db = db

    def parsetodb(self, filename, db=client.plotcsvdb):
        '''
        Takes a file path opens the file with the CSVDictReader module and
        parses every row of the CSV file into a MongoDB Database.
        Furthermore the CSV dialect is tried to be read with the
        csv.Sniffer function.

        The headernames are parsed and whitespaces get replaced with
        underscores in order to avoid problems with the web interface.


        :param client: the pymongo client that was created in the init method
        :type client: <class 'pymongo.database.Database'>

        :param db: the pymongo database that was created in the init method
        :type db: <class 'pymongo.database.Database'>

        >>> testobject = CsvParser()
        >>> isinstance(testobject.parsetodb('csv_to_parse.csv'),object)
        True
        '''

        with open(filename, 'rb') as f:

            dialect = csv.Sniffer().sniff(f.read(1024))
            f.seek(0)

            reader = csv.DictReader(f, dialect=dialect)
            reader.fieldnames = [field.replace(" ", "_") for field in reader.fieldnames]
            # replaes the whitespaces in the fieldnames to avoid problems with
            # http requests later

            for row in reader:
                db.csvcollection.insert(row)


if __name__ == '__main__':
    import doctest
    doctest.testmod()

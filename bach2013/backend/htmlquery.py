# -*- coding: utf-8 -*-
"""
Takes the MongoDB Search Query of an HTML Interface and hands the query to 
a MongoDB Rest Interface. The results of the query are saved and are accessable
in a JSON Object
"""

import collections
import urllib2
import urllib
import json


def convert(data):
    '''
    Takes a Dictionary with unicoded keys and values, and returns them 
    in cleaned form. (Without unicode strings)
    '''
    if isinstance(data, basestring):
        return str(data)
    elif isinstance(data, collections.Mapping):
        return dict(map(convert, data.iteritems()))
    elif isinstance(data, collections.Iterable):
        return type(data)(map(convert, data))
    else:
        return data


#params = '{"Country":"Austria"}'


def make_query(query):

    url = 'http://localhost:27080/plotcsvdb/csvcollection/_find?criteria='

    #Sleepy Mongoose needs to be running tu be able  to run the query 

    params = query #The search query comes from the Browser
    #The search query is handed to to a MongoDB REST interface. 

    hide_id_field = ';fields={"_id":0}'
    request = urllib2.Request (url +urllib.quote(params)+hide_id_field, None) 
    #Escape special Characters of the params attribute for the HTML Query  

    response = urllib2.urlopen (request)


    query_results = json.load(response)
    #Loads the retrieved HTML Page into a JSON Object

    cleaned_query_results = convert(query_results) 
    #cleans the unicode strings in the JSON Object

    return(cleaned_query_results)
    

resultset = make_query('{"Country":"Austria"}')
print resultset['results']




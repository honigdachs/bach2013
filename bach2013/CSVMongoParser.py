# TODO: encoding missing
# TODO: header exists two times
'''
Created on 08.06.2013

@author: matyas
'''
'''
Created on 24.05.2013

@author: matyas
'''
import csv

from pymongo import MongoClient

__version__ = '0.0.1' # TODO: take care about version numbers.

class CsvMongoDBParser(object):

    c = MongoClient(host='localhost', port=3141) # TODO: bad naming.

    def __init__(self, filename, db=c.climatechangedb):

        f = open(filename, 'rb') # TODO: use the 'with' statement instead
        try:
            #trying to read csv dialect
            dialect = csv.Sniffer().sniff(f.read(1024))
            f.seek(0)

            reader = csv.DictReader(f, dialect=dialect)

            self.rownames = reader.fieldnames

            print 'I think the row names are:\n%s\n' % (self.rownames) # TODO: use a logger instead of a print.
        finally:
            f.close() # TODO: might raise an exception if there is and error in the 'try' statement.
            print("read rownames!")
            return # TODO: why an empty return?

    def parsetodb(self, filename, db=c.climatechangedb):
        '''
        :param filename:
        :type filename:
        
        etc. 
        My docstring::

            >>> import math

            >>> math.pow(2,2)
            4.0
        '''

        f = open(filename, 'rb') # TODO: use the with statement
        try:
            dialect = csv.Sniffer().sniff(f.read(1024))
            f.seek(0)

            reader = csv.DictReader(f, dialect=dialect)

            for row in reader:
                db.climatecountries.insert(row)

        # TODO: log errors in an except clause!

        finally:
            f.close()

            print("loaded to database!")

        return # emptry return


if __name__ == '__main__':
    import doctest
    doctest.testmod()